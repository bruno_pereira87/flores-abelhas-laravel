<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlorMes extends Model
{
  use HasFactory;
  protected $table = 'flores_meses';
  public $timestamps = false;

  public static function insertIntoFloresMeses($flor_id,$meses){
    if(is_array($meses) && count($meses) > 0) {
      foreach($meses as $mes){
        $florMes = new FlorMes();
        $florMes->flor_id = $flor_id;
        $florMes->mes_id = $mes;

        $florMes->save();
      }
    }
    else{
      $florMes = new FlorMes();
      $florMes->flor_id = $flor_id;
      $florMes->mes_id = 0;

      $florMes->save();
    }
  }
}
