<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlorAbelha extends Model
{
  use HasFactory;
  protected $table = 'flores_abelhas';
  public $timestamps = false;

  public static function insertIntoFloresAbelhas( $flor_id,$abelhas ){

    foreach($abelhas as $abelha){
      $florAbelha = new FlorAbelha();
      $florAbelha->flor_id = $flor_id;
      $florAbelha->abelha_id = $abelha;

      $florAbelha->save();
      // parent::insert(
      //   ['flor_id','abelha_id'],
      //   [$flor_id, intval($abelha)],
      //   'flores_abelhas'
      // );
    }
  }
}
