<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Abelha;
use App\Models\Flor;
use App\Models\Mes;

class HomeController extends Controller
{
  public function index(){
    $dados = [
      'title'=>'Calendário de Flores',
      'main_title'=>'Calendário de Flores',
      'css'=>'home',
      'header_type' => 'list'
    ];

    $dados['abelhas'] = Abelha::all();
    $dados['meses'] = Mes::all();
    $dados['flores'] = Flor::all();

    return view('home',$dados);
  }

  public function flores(Request $request){
    $token =
    $meses = $request->input('meses');
    $abelhas = $request->input('abelhas');

    if(isset($meses) && !empty($meses) && isset($abelhas) && !empty($abelhas)){
        $query = "SELECT DISTINCT f.* FROM flores f INNER JOIN flores_meses fm ON f.id = fm.flor_id ".
        "INNER JOIN flores_abelhas fa ON f.id = fa.flor_id ".
        "WHERE f.id = fm.flor_id AND fm.mes_id IN ($meses) OR f.id = fa.flor_id ".
        "AND fa.abelha_id IN ($abelhas)";
    }else if(isset($meses) && !empty($meses)){
        $query = "SELECT DISTINCT f.* FROM flores f INNER JOIN flores_meses fm ON f.id = fm.flor_id ".
        "WHERE f.id = fm.flor_id AND fm.mes_id IN ($meses) ";
    }else if(isset($abelhas) && !empty($abelhas)){
        $query = "SELECT DISTINCT f.* FROM flores f INNER JOIN flores_abelhas fa ON f.id = fa.flor_id ".
        "WHERE f.id = fa.flor_id AND fa.abelha_id IN ($abelhas) ";
    }
    else {
        $query = "SELECT * FROM flores";
    }

    $flores = DB::select(DB::raw($query));

    echo json_encode($flores);
  }

  public function flor_abelhas($flor_id){
    $sql = "SELECT DISTINCT a.nome FROM abelhas a INNER JOIN flores_abelhas fa ON a.id = fa.abelha_id ".
    "WHERE fa.flor_id = $flor_id";

    $query_abelhas = DB::select(DB::raw($sql));

    return $query_abelhas;
  }
}
