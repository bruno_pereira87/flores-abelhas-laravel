<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Abelha;
use App\Models\Flor;
use App\Models\FlorAbelha;
use App\Models\FlorMes;
use App\Models\Mes;
class CadastrarController extends Controller
{
  public function abelha(Request $request){
    $dados = [
      'css' => 'cadastrar',
      'title' => 'Cadastre abelhas',
      'main_title' => 'Cadastre abelhas'
    ];


    return view('cadastrar_abelha',$dados);
  }

  public function flor(){
    $dados = [
      'css' => 'cadastrar',
      'title' => 'Cadastre flores',
      'main_title' => 'Cadastre flores'
    ];

    $dados['meses'] = Mes::all();
    $dados['abelhas'] = Abelha::all();

    return view('cadastrar_flor',$dados);
  }

  public function adicionarAbelha(Request $request){
    $nome = $request->input('nome');
    $especie = $request->input('especie');

    if(empty($nome) || empty($especie)){
      return redirect()->route('abelha')->with('erro', 'Preencha todos os campos!');
    }
    else{
      $abelha = new Abelha();
      $abelha->nome = $nome;
      $abelha->especie = $especie;
      $abelha->save();

      return redirect()->route('home')->with('success','Abelha cadastrada com sucesso');
    }
  }

  public function adicionarFlor(Request $request){
    $nome = $request->input('nome');
    $especie = $request->input('especie');
    $descricao = $request->input('descricao');
    $meses = $request->input('meses');
    $imagem = 'flower_icon.png';
    $abelhas_id = explode(',',$request->input('abelhas_id'));

    $file = $request->file('imagem');

    if($file){
      $imagem = $this->salvaImagem($file);
      if(empty($imagem)){
        return redirect()->route('flor')->with('erro','Tipo de arquivo inválido...');
      }

    }

    $flor = new Flor();
    $flor->nome = $nome;
    $flor->especie = $especie;
    $flor->descricao = $descricao;
    $flor->imagem = $imagem;
    $flor->save();

    $flor_id = $flor->id;

    if(isset($flor_id) && !empty($flor_id)){
      FlorMes::insertIntoFloresMeses($flor_id,$meses);
      FlorAbelha::insertIntoFloresAbelhas($flor_id,$abelhas_id);
    }

    return redirect()->route('home')->with('success','Flor cadastrada com sucesso');
  }

  private function salvaImagem($imagem){
    $tipos = ['image/jpg','image/png','image/jpeg'];
    $nome = md5(time(). rand(0, 9999)).'.jpg';

    if(in_array($imagem->getMimeType(), $tipos)){
        $path = $imagem->storeAs('flores',$nome,['disk' => 'public']);
        // $path_splited = explode('/',$path);
        // $nome = $path_splited[1];
        // $path = public_path('/img/flores');
        // $imagem->move($path,$nome);

        return $nome;
    }

    return '';
  }
}

