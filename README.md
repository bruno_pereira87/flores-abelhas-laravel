## Instalação

Você pode clonar este repositório OU baixar o .zip

Ao descompactar, é necessário rodar o **composer** pra instalar as dependências e gerar o _autoload_.

Vá até a pasta do projeto, pelo _prompt/terminal_ e execute:

> composer install

Depois é só aguardar.

## Execução

Para iniciar o servidor e rodar o projeto, execute no prompt/terminal o comando:

> php artisan serve
