@extends('template')

@section('title','Calendário de Flores')
@section('css','home')

@section('content')

<div class="conteudo">
  <section class="calendario_flores">
    <script>
      var abelhas = @json($abelhas)
    </script>
    <script>
      var meses = @json($meses)
    </script>
    <script>
      var flores = @json($flores)
    </script>
    <section class="top">
      <h1 class="page_title">Calendário de Flores</h1>
      <div class="link_buttons">
          <a href="{{url('cadastrar/flor')}}" class="btn btn-primary">Cadastrar Flor</a>
          <a href="{{url('cadastrar/abelha')}}" class="btn btn-primary">Cadastrar Abelha</a>
      </div>
      <div class="menu_mobile">
        <button type="button" class="btn-close">X</button>
        <div class="link_buttons">
            <a href="{{url('cadastrar/flor')}}" class="btn btn-primary">Cadastrar Flor</a>
            <a href="{{url('cadastrar/abelha')}}" class="btn btn-primary">Cadastrar Abelha</a>
        </div>
      </div>
      <button class="mobileButton" aria-label="Menu"></button>
    </section>

    <section class="calendar">
      <p class="description">Neste calendário encontram-se diversas flores. </p>
      <p class="description">Podem ser agrupadas pelos meses que  florescem e/ou pelo
        tipo de abelha que poliniza a flor.
      </p>

      <div class="form-group abelha_input">
        @csrf
        <label for="abelha">Selecione as abelhas</label>
        <input type="hidden" name="abelhas_id" />
        <div class="abelha_groups">

          <select class="abelha_select form-control" busca name="abelha" id="abelha">
            <option value="">...Selecione uma abelha</option>

              @foreach($abelhas as $abelha )

            <option value="{{$abelha['id']}}">{{$abelha['nome']}}({{$abelha['especie']}})</option>

              @endforeach

          </select>
          <div class="abelhas">
          </div>
        </div>
        <div class="month_buttons">

        @foreach($meses as $mes )

          <div class="checkbutton">
            <input type="checkbox" busca name="meses[]" value="{{$mes['id']}}" id="{{$mes['abreviacao']}}">
            <label for="{{$mes['abreviacao']}}">{{$mes['abreviacao']}}</label>
          </div>

          @endforeach

          </div>
      </div>
      <div class="flores">

      @if(isset($flores) && count($flores) > 0)
        @foreach($flores as $flor)

        <div class="flor">
          <img src="{{URL::asset('img/flores/'.$flor["imagem"])}}">
          <span>{{$flor['nome']}}</span>
        </div>

        @endforeach
      @endif
      </div>
      <div class="modal">
        <div class="flor_detalhes">
          <img id="imagem_flor" />
          <div class="dados_flor">
            <h4 id="nome_flor"></h4>
            <p id="descricao_flor"></p>
            <h4>Abelhas</h4>
            <div id="abelhas_flor">
            </div>
          </div>

        </div>
      </div>
    </section>
  </section>
</div>
@endsection
