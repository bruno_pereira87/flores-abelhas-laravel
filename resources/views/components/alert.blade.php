@if(session('success'))
  <p class="alert alert-success">{{session('success')}}</p>
@endif

@if(session('erro'))
  <p class="alert alert-erro">{{session('erro')}}</p>
@endif

@if(session('semcurso'))
  <p class="alert alert-semcurso">{{session('semcurso')}}</p>
@endif
