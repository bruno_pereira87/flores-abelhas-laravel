<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=no">
  <title>@yield('title')</title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Serif:wght@700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('css/fontawesome-all.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('css/estilos.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @if(isset($css) && !empty($css))
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/'.$css.'.css')}}">
  @endif

</head>
<body>
  <main>
    @yield('content')
  </main>
    <footer class="rodape">
			<script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
			<script src="{{URL::asset('js/jquery.star-rating-svg.min.js')}}"></script>
			<script src="{{URL::asset('js/script.js')}}"></script>
    </footer>
</body>
</html>
