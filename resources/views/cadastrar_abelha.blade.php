@extends('template')

@section('title',$main_title)
@section('css','cadastrar')

@section('content')
<section class="register register_bee">
  <script>
    var flores = undefined
  </script>

  <div class="conteudo">
    <x-alert  />
    @if(isset($main_title))
      <h1 class="page_title">{{$main_title}}</h1>
    @endif
    <p class="page_description">Cadastre as abelhas</p>
    <form method="POST" action="{{url('/cadastrar/abelha')}}">
      @csrf
        <div class="form-group">
          <label for="nome">Nome</label>
          <input type="text" class="form-control" name="nome" id="nome"
          {{isset($nome) ? 'value="'+$nome+'"' : ''}} >
        </div>
        <div class="form-group">
          <label for="especie">Espécie</label>
          <input type="text" class="form-control" name="especie" id="especie"
          {{isset($especie) ? 'value="'+$especie+'"' : ''}} >
        </div>
        <div class="action_buttons">
          <button type="button" class="btn btn-primary btn-cancelar">Cancelar</button>
          <button class="btn btn-secondary btn-cadastrar">Cadastrar Abelha</button>
        </div>

    </form>
  </div>
</section>
@endsection
