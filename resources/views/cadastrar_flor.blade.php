@extends('template')

@section('title',$main_title)
@section('css','cadastrar')

@section('content')
<section class="register register_flower">
  <script>
    var abelhas = @json($abelhas)
  </script>
  <script>
    var flores = undefined
  </script>
  <div class="conteudo">

    <x-alert  />
    @if(isset($main_title))
      <h1 class="page_title">{{$main_title}}</h1>
    @endif

    <p class="page_description">Cadastre as flores de acordo com os meses em que ela floresce</p>
    <form method="POST" action="{{url('cadastrar/flor')}}" enctype="multipart/form-data">
      @csrf
    <div class="photo_row">
      <div class="inputs_text">
        <div class="form-group">
          <label for="nome">Nome</label>
          <input type="text" class="form-control" name="nome" id="nome"
          {{isset($nome) ? 'value="'+$nome+'"' : ''}} >
        </div>
        <div class="form-group">
          <label for="especie">Espécie</label>
          <input type="text" class="form-control" name="especie" id="especie"
          {{isset($especie) ? 'value="'+$especie+'"' : ''}} >
        </div>
      </div>

      <div class="form-group input_image">
        <div class="image-form">
          <img src="{{URL::asset('img/photo_default.svg')}}" id="img-input" class="img-thumbnail">
          <input type="file" name="imagem" id="imagem">
          <label id="image-label" for="imagem">ESCOLHA UMA IMAGEM</label>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="descricao">Descrição</label>
      <input type="text" class="form-control" name="descricao" id="descricao"
      {{isset($descricao) ? 'value="'+$descricao+'"' : ''}} >
    </div>
    <div class="months">
        <p>Quais meses a flor irá nascer?</p>
        <div class="month_buttons">
          <?php
            foreach($meses as $mes):
              ?>
          <div class="checkbutton">
            <input type="checkbox" name="meses[]" value="{{$mes['id']}}" id="{{$mes['abreviacao']}}">
            <label for="{{$mes['abreviacao']}}">{{$mes['abreviacao']}}</label>
          </div>
          <?php
            endforeach
          ?>
        </div>

      </div>
      <div class="form-group abelha_input cadastrar_abelha_input">
        <label for="abelha">Selecione as abelhas que polinizam esssas flores</label>
        <input type="hidden" name="abelhas_id" />

        <div class="abelhas_group">
          <div class="abelhas">
          </div>
          <button class="btn-icon" type="button" id="btn-abelha">+</button>
        </div>
        <select class="abelha_select" name="abelha" id="abelha">
        <option value="">...Selecione uma abelha</option>

        @foreach($abelhas as $abelha )
          <option value="{{$abelha['id']}}">{{$abelha['nome']}}({{$abelha['especie']}})</option>
        @endforeach

        </select>
      </div>
      <div class="action_buttons">
        <button type="button" class="btn btn-primary btn-cancelar">Cancelar</button>
        <button class="btn btn-secondary btn-cadastrar">Cadastrar Flor</button>
      </div>

    </form>
  </div>
</section>
@endsection
