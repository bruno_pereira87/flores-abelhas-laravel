<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CadastrarController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index'])->name('home');
Route::post('/home/flores',[HomeController::class,'flores']);
Route::get('/home/flor_abelhas/{flor_id}',[HomeController::class,'flor_abelhas']);

Route::get('/cadastrar/flor',[CadastrarController::class,'flor'])->name('flor');
Route::get('/cadastrar/abelha',[CadastrarController::class,'abelha'])->name('abelha');
Route::post('/cadastrar/flor',[CadastrarController::class,'adicionarFlor']);
Route::post('/cadastrar/abelha',[CadastrarController::class,'adicionarAbelha']);
